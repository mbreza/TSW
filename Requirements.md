|||
|---|---|
| Identyfikator: | temperatura_pobranie |
|Priorytet: | M |
|Nazwa: | Pobranie temperatury |
|Opis: | Pobranie z czujnika temperatury jej dokładnej wartości |
|Dane wejściowe: | Brak |
|Warunki początkowe: | Podłączony czujnik temperatury |
|Warunki końcowe:| Podłączony czujnik temperatur, pobrana temperatura |
|Sytuacje wyjątkowe: | Awaria czujnika temperatury, skrajne temperatury |
|Efekty uboczne: | Brak |
|Czynności równoczesne: | Brak |
|Udziałowiec: | --- |
|Powiązania z innymi wymaganiami: | dioda_cold, dioda_opt, dioda_hot |

|||
|---|---|
| Indentyfikator: | dioda_cold |
|Priorytet: | S |
|Nazwa: | Dioda zimno|
|Opis: | W zakresie temperatur od 0 do 15 zapala się niebieska dioda |
|Dane wejściowe: | Dane z czytnika temperatury |
|Warunki początkowe: | Podłączona niebieska zielona i czerwona dioda |
|Warunki końcowe: | Zapalona niebieska dioda, pozostałe zgaszone |
|Sytuacje wyjątkowe: | Awaria diod, temperatura poniżej 0 stopni |
|Efekty uboczne: | Brak |
|Czynności równoczesne: | Brak |
|Udziałowiec: | --- |
|Powiązania z innymi wymaganiami: | temperatura_pobranie, dioda_opt, dioda_hot  |

|||
|---|---|
| Indentyfikator: | dioda_opt |
|Priorytet: | S |
|Nazwa: | Dioda optymalna |
|Opis: | W zakresie temperatur od 16 do 25 zapala się zielona dioda |
|Dane wejściowe: | Dane z czytnika temperatury |
|Warunki początkowe: | Podłączona niebieska zielona i czerwona dioda|
|Warunki końcowe: | Zapalona zielona dioda, pozostałe zgaszone |
|Sytuacje wyjątkowe: | Awaria diod |
|Efekty uboczne: | Brak |
|Czynności równoczesne: | Brak |
|Udziałowiec: | --- |
|Powiązania z innymi wymaganiami: | temperatura_pobranie, dioda_cold, dioda_hot |

|||
|---|---|
| Indentyfikator: | dioda_hot |
|Priorytet: | S |
|Nazwa: | Dioda gorąco|
|Opis: | W zakresie temperatur od 26 do 50 zapala się zielona dioda |
|Dane wejściowe: | Dane z czytnika temperatury |
|Warunki początkowe: | Podłączona niebieska zielona i czerwona dioda |
|Warunki końcowe: | Zapalona czerwona dioda, pozostałe zgaszone |
|Sytuacje wyjątkowe: | Awaria diod, temperatura ponad 50 stopni |
|Efekty uboczne: | Brak |
|Czynności równoczesne: | Brak |
|Udziałowiec: | --- |
|Powiązania z innymi wymaganiami: | temperatura_pobranie, dioda_1, dioda_2 |

|||
|---|---|
|Indentyfikator: | alarm_on |
|Priorytet: | M |
|Nazwa: | Alarm włączenie |
|Opis: |Uzbrajanie alarmu|
|Dane wejściowe: | Dotknięcie czujnika dotykowego |
|Warunki początkowe: | Alarm wyłączony, dioda statusu alarmu wyłączona |
|Warunki końcowe: | Alarm włączony, dioda status alarmu włączona |
|Sytuacje wyjątkowe: | Awaria czujnika dotykowego |
|Efekty uboczne: | Brak |
|Czynności równoczesne: | Aktywacja diody statusu alarmu |
|Udziałowiec: | --- |
|Powiązania z innymi wymaganiami: | alarm_off, buzzer_on, buzzer_off |

|||
|---|---|
|Indentyfikator: | alarm_off |
|Priorytet: | M |
|Nazwa:  |Alarm wyłączenie|
|Opis: |Rozbrajanie alarmu |
|Dane wejściowe: | Dotknięcie czujnika dotykowego |
|Warunki początkowe: | Alarm włączony, dioda statusu alarmu włączona |
|Warunki końcowe: | Alarm wyłączony, dioda statusu alarmu wyłączona |
|Sytuacje wyjątkowe: | Awaria czujnika dotykowego |
|Efekty uboczne: | Brak |
|Czynności równoczesne: | Dezaktywacja diody statusu alarmu |
|Udziałowiec: | --- |
|Powiązania z innymi wymaganiami: | alarm_on, buzzer_on, buzzer_off |

|||
|---|---|
|Indentyfikator: | buzzer_on |
|Priorytet: | M |
|Nazwa: | Otwarcie drzwi|
|Opis: | Aktywacja buzzera za pomocą guzika |
|Dane wejściowe: | Informacja o naciśnięciu guzika |
|Warunki początkowe: | Uzbrojony alarm, buzzer nieaktywny |
|Warunki końcowe: | Uzbrojony alarm, buzzer aktywny |
|Sytuacje wyjątkowe: | Awaria buzzera, guzika |
|Efekty uboczne: | Brak |
|Czynności równoczesne: | Wciśnięcie guzika, aktywacja buzzera |
|Udziałowiec: | --- |
|Powiązania z innymi wymaganiami: | alarm_on, alarm_off, buzzer_off |

|||
|---|---|
|Indentyfikator: | buzzer_off |
|Priorytet: | M |
|Nazwa: | Zamknięcie drzwi|
|Opis: | Dezaktywacja buzzera za pomocą guzika |
|Dane wejściowe: | Informacja o zwolnieniu guzika |
|Warunki początkowe: | Uzbrojony alarm, buzzer aktywny |
|Warunki końcowe: | Uzbrojony alarm, buzzer nieaktywny |
|Sytuacje wyjątkowe: | Awaria buzzera, guzika |
|Efekty uboczne: | Brak |
|Czynności równoczesne: | Zwolnienie guzika, dezaktywacja buzzera |
|Udziałowiec: | --- |
|Powiązania z innymi wymaganiami: | alarm_on, alarm_off, buzzer_on |
