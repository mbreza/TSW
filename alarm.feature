Feature: Testowanie alarmu

Scenario: Wlaczanie alarmu
    Given smartHome z wylaczonym alarmem
    When Aktywujemy alarm
    Then Alarm wlaczony

Scenario: Wylaczanie alarmu
    Given smartHome z wlaczonym alarmem
    When Dezaktywujemy alarm
    Then Alarm wylaczony

Scenario: Aktywacja alarmu
    Given smartHome z wlaczonym alarmem, wylaczonym buzzerem
    When Aktywujemy buzzer
    Then Buzzer jest aktywny

Scenario: Dezatywacja alarmu
    Given smartHome z wlaczonym alarmem, wlaczonym buzzerem
    When Dezaktywujemy buzzer
    Then Buzzer jest nie aktywny




