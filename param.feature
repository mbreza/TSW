Feature: Testowanie temperatury z parametrami

Scenario Outline: Testowanie temperatury
  Given Inicjalizacja smatHome
  When wczytujemy temerature <temp>
  Then <dioda> sie zapala

  Examples: Temp
  | temp          | dioda        |
  | 10            | diod_cold    |
  | 20            | diod_opt     |
  | 30            | diod_hot     |