import mraa
import time
import math
            
class SmartHome():
    
    def __init__(self, alarm):
        self.alarm = alarm
        self.alarmOnn = False
        self.temp_pin = mraa.Aio(0)
        self.button_pin = mraa.Gpio(2)
        self.button_pin.dir(mraa.DIR_IN)
        self.touch_pin = mraa.Gpio(3)
        self.touch_pin.dir(mraa.DIR_IN)
        self.diod_cold = mraa.Gpio(4)
        self.diod_cold.dir(mraa.DIR_OUT)
        self.diod_opt = mraa.Gpio(5)
        self.diod_opt.dir(mraa.DIR_OUT)
        self.diod_hot = mraa.Gpio(6)
        self.diod_hot.dir(mraa.DIR_OUT)
        self.diod_alarm = mraa.Gpio(7)
        self.diod_alarm.dir(mraa.DIR_OUT)
        self.buzzer_pin = mraa.Gpio(8)         
        self.buzzer_pin.dir(mraa.DIR_OUT)

    def lights_temp(self):
        B=3975
        
        read_temp_pin = self.temp_pin.read()
        resistance = (1023-read_temp_pin)*10000.0/read_temp_pin
        temp = 1/(math.log(resistance/10000.0)/B+1/298.15)-273.15
        print "Temperature now is " + str(temp)

        self.lights_diod(temp)

    def lights_diod(self, temp):
        if(temp >= 0 and temp <=15.99):
            self.diod_opt.write(0)
            self.diod_hot.write(0)
            self.diod_cold.write(1)

        if(temp >= 16 and temp <=25.99):
            self.diod_cold.write(0)
            self.diod_hot.write(0)
            self.diod_opt.write(1)

        if(temp >= 26 and temp <=50):
            self.diod_cold.write(0)
            self.diod_opt.write(0)
            self.diod_hot.write(1)

    def alarmOn(self):
        self.alarm = True
        self.diod_alarm.write(1)

    def alarmOff(self):
        self.alarm = False
        self.diod_alarm.write(0)

    def buzzerOn(self):
        self.buzzer_pin.write(1)
        self.alarmOnn = True

    def buzzerOff(self):
        self.buzzer_pin.write(0)
        self.alarmOnn  = False


    def runAlarm(self):              
        if(self.touch_pin.read() == 1):
            if(self.alarm == True):
                self.alarmOff()
            else:
                self.alarmOnn()
            time.sleep(5)
        while(self.alarm == True and self.button_pin.read() == 1):
            self.buzzerOn()
        self.buzzerOff

    
    def run(self):
        while True:
            self.lights_temp()
            self.runAlarm()



#sh = SmartHome()
#sh.run()
