from behave import *
from sh import *
import time

#wlaczenie alarmu
@given('smartHome z wylaczonym alarmem')
def step_impl(context):
    context.dom = SmartHome(False)

@when('Aktywujemy alarm')
def step_impl(context):
    context.dom.alarmOn()

@then('Alarm wlaczony')
def step_impl(context):
    assert context.dom.alarm is True

#wylaczenie alarmu
@given('smartHome z wlaczonym alarmem')
def step_impl(context):
    context.dom = SmartHome(True)

@when('Dezaktywujemy alarm')
def step_impl(context):
    #global temp
    context.dom.alarmOff()

@then('Alarm wylaczony')
def step_impl(context):
    assert context.dom.alarm is False

#wlaczenie buzzera
@given('smartHome z wlaczonym alarmem, wylaczonym buzzerem')
def step_impl(context):
    context.dom = SmartHome(True)

@when('Aktywujemy buzzer')
def step_impl(context):
    context.dom.buzzerOn()

@then('Buzzer jest aktywny')
def step_impl(context):
    assert context.dom.alarmOnn is True
    assert context.dom.buzzer_pin.read() is 1
    context.dom.buzzerOff()

#wylaczenie buzzera
@given('smartHome z wlaczonym alarmem, wlaczonym buzzerem')
def step_impl(context):
    context.dom = SmartHome(True)
    context.dom.buzzerOn()

@when('Dezaktywujemy buzzer')
def step_impl(context):
    context.dom.buzzerOff()

@then('Buzzer jest nie aktywny')
def step_impl(context):
    assert context.dom.alarmOnn is False
    assert context.dom.buzzer_pin.read() is 0
    


            
    

