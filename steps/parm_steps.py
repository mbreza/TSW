from behave import *
import time

#temp param
@given('Inicjalizacja smatHome')
def step_impl(context):
    context.dom = SmartHome(False)

@when('wczytujemy temerature {temp}')
def step_impl(context, temp):
    context.dom.lights_diod(temp)

@then('{dioda} sie zapala')
def step_impl(context, dioda):
    assert context.dom.dioda.read() is 1