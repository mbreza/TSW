from behave import *
from sh import *
import time

#temp zimno
@given('Inicjalizacja smatHome_1')
def step_impl(context):
    context.dom = SmartHome(False)

@when('Wczytujemy temerature z przedzialu 0-15')
def step_impl(context):
    context.dom.lights_diod(10)

@then('Zapala sie diod_cold')
def step_impl(context):
    assert context.dom.diod_cold.read() is 1


#temp srednio
@given('Inicjalizacja smatHome_2')
def step_impl(context):
    context.dom = SmartHome(False)

@when('Wczytujemy temerature z przedzialu 16-25')
def step_impl(context):
    context.dom.lights_diod(20)

@then('Zapala sie diod_opt')
def step_impl(context):
    assert context.dom.diod_opt.read() is 1

#temp cieplo
@given('Inicjalizacja smatHome_3')
def step_impl(context):
    context.dom = SmartHome(False)

@when('Wczytujemy temerature z przedzialu 26-50')
def step_impl(context):
    context.dom.lights_diod(30)

@then('Zapala sie diod_hot')
def step_impl(context):
    assert context.dom.diod_hot.read() is 1