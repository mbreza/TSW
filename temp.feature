Feature: Testowanie temperatury

Scenario: Temperatura zimna
    Given Inicjalizacja smatHome_1
    When Wczytujemy temerature z przedzialu 0-15
    Then Zapala sie diod_cold

Scenario: Temperatura srednia
    Given Inicjalizacja smatHome_2
    When Wczytujemy temerature z przedzialu 16-25
    Then Zapala sie diod_opt

Scenario: Temperatura ciepla
    Given Inicjalizacja smatHome_3
    When Wczytujemy temerature z przedzialu 26-50
    Then Zapala sie diod_hot