import unittest
from sh import *

class TestUM(unittest.TestCase):
     
    def setUp(self):
        pass
 
    def test_alarmOn(self):
        smartHome = SmartHome(False)
        smartHome.alarmOn()
        self.assertEqual( smartHome.alarm, True)

    def test_alarmOff(self):
        smartHome = SmartHome(True)
        smartHome.alarmOff()
        self.assertEqual( smartHome.alarm, False)

    def test_buzzerOn(self):
        smartHome = SmartHome(True)
        smartHome.buzzerOn()
        self.assertEqual( smartHome.alarmOnn, True)
        self.assertEqual( smartHome.buzzer_pin.read(), 1)
        smartHome.buzzerOff()

    def test_buzzerOff(self):
        smartHome = SmartHome(True)
        smartHome.buzzerOn()
        smartHome.buzzerOff()
        self.assertEqual( smartHome.alarmOnn, False)
        self.assertEqual( smartHome.buzzer_pin.read(), 0)

    def test_diodCold(self):
        smartHome = SmartHome(False)
        smartHome.lights_diod(10)
        self.assertEqual( smartHome.diod_cold.read(), 1)

    def test_diodOpt(self):
        smartHome = SmartHome(False)
        smartHome.lights_diod(20)
        self.assertEqual( smartHome.diod_opt.read(), 1)

    def test_diodHot(self):
        smartHome = SmartHome(False)
        smartHome.lights_diod(30)
        self.assertEqual( smartHome.diod_hot.read(), 1)
 
if __name__ == '__main__':
    unittest.main()
